# SSP projekt

Tento repozitář slouží jako dokumentační průvodce pro projekt do SSP (správy softwarových projektů) Obsahuje informace o použití, instalaci a konfiguraci projektu.

## Instalace

1. Klonujte tento repozitář na svůj lokální stroj.
2. Nainstalujte závislosti pomocí příkazu `pip install -r requirements.txt`.

## Použití

1. Otevřete složku s mkdocs `cd mkdocs`.
2. Pro lokální vývoj spusťte MKDocs server pomocí příkazu `mkdocs serve`.
3. Pro generování statických stránek použijte příkaz `mkdocs build`.


Odkaz na stránku: [link to site](https://bercek71.gitlab.io/ssp-project)
