# Přispívání

Chcete-li přispět k tomuto projektu, máte k dispozici několik možností:

1. **Nahlášení chyb:** Pokud narazíte na chybu nebo máte návrh na vylepšení, vytvořte prosím nový issue v sekci Issues tohoto repozitáře.
2. **Opravy chyb:** Pokud jste schopni opravit chybu nebo implementovat novou funkcionalitu, můžete vytvořit novou pull request. Prosím, ujistěte se, že váš kód odpovídá přijatelným standardům kvality.
3. **Vylepšení dokumentace:** Pokud máte nápad na vylepšení dokumentace, přidejte prosím nové informace nebo opravte stávající obsah a vytvořte pull request.

Před odesláním pull requestu se ujistěte, že jste provedli dostatečné testování a že váš kód je bez chyb.

Děkujeme vám za váš příspěvek k tomuto projektu!
