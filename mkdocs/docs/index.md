# Dokumentace projektu SSP - Správa Softwarových Projektů

Tento repozitář slouží k vytvoření a správě dokumentace projektu v rámci předmětu SSP (Správa Softwarových Projektů). Projekt využívá generátor statických stránek ve spojení s kontinuální integrací (CI), která generuje HTML stránky z Markdown zdrojů a zahrnuje také test kvality Markdown souborů. Součástí CI je také automatické nasazení webových stránek.

## Instalace

1. Klonujte tento repozitář na svůj lokální stroj pomocí příkazu `git clone`.
2. Nainstalujte závislosti a prostředí pro vývoj pomocí pokynů uvedených v návodu projektu.

## Použití

1. Upravte obsah Markdown souborů podle potřeb projektu.
2. Proveďte změny, commitněte je a pushněte do repozitáře na GitLabu.

## Kontinuální integrace

Projekt využívá GitLab CI pro kontinuální integraci. CI pipeline obsahuje následující úlohy:

1. Testování kvality Markdown stránek.
2. Generování HTML stránek z Markdown zdrojů.


